#!/bin/sh

# Testing, run in my git.git:
# Linux:
#  OPT_JOBS=3 OPT_JOB_NAME=gcc45 ../git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh
#  OPT_JOBS=3 OPT_JOB_NAME=gcc45 OPT_TESTS='t1450*.sh t4212*.sh t9300*.sh' ../git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh
#  OPT_JOBS=32 OPT_JOB_NAME=gcc102 ../git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh
#  OPT_JOBS=16 OPT_JOB_NAME=gcc112 OPT_NICE=1 ../git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh
# AIX:
#  OPT_JOB_NAME='gcc119' OPT_TESTS='t1450*.sh t4212*.sh t9300*.sh' ../git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh
# Solaris:
#  OPT_JOB_NAME='gcc210' OPT_TESTS='t[0-9]*.sh' OPT_JOBS=10 ../git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh
# OpenBSD:
#  OPT_JOB_NAME='gcc302' OPT_TESTS='t13[0-9]*.sh' ../git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh
# General:
#  OPT_JOB_NAME='gcc112' OPT_REVISION=git/pu OPT_JOBS=20 ../git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh
#  OPT_JOB_NAME='gcc112' OPT_REVISION=git/next OPT_TESTS='t00[0-9]*.sh' OPT_JOBS=20 ../git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh
# Compile-only with OPT_TESTS=false:
#  OPT_JOBS=4 OPT_JOB_NAME=gcc211 OPT_TESTS=false ~/g/git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh
# Re-compile only:
#  OPT_JOBS=4 OPT_JOB_NAME=gcc211 OPT_TESTS=false OPT_CMD_POST_CHECKOUT='git clean -dxf' ~/g/git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh
#  OPT_JOBS=4 OPT_JOB_NAME=gcc211 OPT_TESTS=false OPT_CMD_POST_CHECKOUT='git clean -dxf' OPT_CMD_BUILD='gmake -j1 V=1' ~/g/git-gitlab-ci/ci/gitlab/run-on-gcc-farm.sh

set -e

if test -z "$OPT_HOST"
then
	# Sanity check
	if ! git rev-parse --verify e83c5163316f89bfbde7d9ab23ca2e25604af290 >/dev/null 2>&1
	then
		echo "You need to run this inside a git.git checkout!"
		exit 1
	fi

	# Config
	DEBUG=
	IN_CI=
	JOB_NAME=gcc112
	GCC_HOSTNAME=$JOB_NAME
	GCC_USER=avar
	TRANSFER_METHOD=auto
	PATH_PREFIX=auto
	REMOTE_REPO_PATH=
	CMD_PRE_CHECKOUT=
	CMD_POST_CHECKOUT=

	# Options (some override config)
	if test -n "$OPT_DEBUG"
	then
		set -x
		DEBUG=$OPT_DEBUG
	fi
	if test -n "$OPT_IN_CI"
	then
		IN_CI=1
	fi
	if test -n "$OPT_JOB_NAME"
	then
		JOB_NAME=$OPT_JOB_NAME
	fi
	if test -n "$OPT_GCC_USER"
	then
		GCC_USER=$OPT_GCC_USER
	fi
	if test -n "$OPT_GCC_HOSTNAME"
	then
		GCC_HOSTNAME=$OPT_GCC_HOSTNAME
	fi
	if test -n "$OPT_TRANSFER_METHOD"
	then
		TRANSFER_METHOD=$OPT_TRANSFER_METHOD
	fi
	if test -n "$OPT_CMD_PRE_CHECKOUT"
	then
		CMD_PRE_CHECKOUT=$OPT_CMD_PRE_CHECKOUT
	fi
	if test -n "$OPT_CMD_POST_CHECKOUT"
	then
		CMD_POST_CHECKOUT=$OPT_CMD_POST_CHECKOUT
	fi
	if test -n "$OPT_CMD_BUILD"
	then
		CMD_BUILD=$OPT_CMD_BUILD
	fi

	# Auto-munge options
	if echo $GCC_HOSTNAME | grep -q -v -F '.'
	then
		GCC_HOSTNAME=$JOB_NAME.fsffrance.org
	fi
	GCC_HOST=$(echo $GCC_HOSTNAME | cut -d '.' -f 1)

	# What revision?
	if test -n "$OPT_REVISION"
	then
		rev_parse=$(git rev-parse $OPT_REVISION)
		echo "Going to test '$OPT_REVISION' (parsed as '$rev_parse')" >&2
		OPT_REVISION=$rev_parse
	elif test -z "$IN_CI"
	then
		rev_parse=$(git rev-parse HEAD)
		echo "Going to test 'HEAD' ($rev_parse) as no explicit '\$OPT_REVISION' was given" >&2
		OPT_REVISION=$rev_parse
	elif test "$(git remote get-url origin 2>/dev/null)" = "git@gitlab.com:git-vcs/git-gitlab-ci.git"
	then
		# In OPT_IN_CI=1!
		#
		# I'm in the git-gitlab-ci repo itself running an ad-hoc
		# test. Using ls-remote here instead of 'git rev-parse
		# git/master' is intentional. If Junio's pushed out a new
		# version I want to re-fetch.
		OPT_REVISION=$(git ls-remote https://gitlab.com/git-vcs/git.git refs/heads/master | cut -f1)
		if ! git cat-file -e $OPT_REVISION 2>/dev/null
		then
			echo "Don't have $OPT_REVISION here. Fetching git.git"
			git fetch git
		fi
	else
		# Also OPT_IN_CI=1!
		#
		# The merge we create always has the git.git push as the LHS
		# of the merge.
		OPT_REVISION=$(git rev-parse HEAD^)
	fi

	if test -n "$IN_CI" && ! git rev-parse --verify $OPT_REVISION >/dev/null 2>&1
	then
		# In my test repo
		git remote add git https://gitlab.com/git-vcs/git.git
		git config remote.origin.tagOpt --no-tags
		git fetch git
	fi

	do_on_remote() {
		what=$1
		ssh -o StrictHostKeyChecking=accept-new $GCC_USER@$GCC_HOSTNAME \
			"\
			LMOD_SH_DBG_ON=1 \
			OPT_WHAT=\"$what\" \
			OPT_HOST=\"$GCC_HOST\" \
			OPT_TRANSFER_METHOD=\"$TRANSFER_METHOD\" \
			OPT_DEBUG=\"$DEBUG\" \
			OPT_NICE=\"$OPT_NICE\" \
			OPT_TESTS=\"$OPT_TESTS\" \
			OPT_JOBS=\"$OPT_JOBS\" \
			OPT_PROVE_STATELESS=\"$OPT_PROVE_STATELESS\" \
			OPT_REVISION=\"$OPT_REVISION\" \
			OPT_PATH_PREFIX=\"$PATH_PREFIX\" \
			OPT_REMOTE_REPO_PATH=\"$REMOTE_REPO_PATH\" \
			OPT_CMD_PRE_CHECKOUT=\"$CMD_PRE_CHECKOUT\" \
			OPT_CMD_POST_CHECKOUT=\"$CMD_POST_CHECKOUT\" \
			OPT_CMD_BUILD=\"$CMD_BUILD\" \
			nice -n 19 bash ${DEBUG:+-x} -" <$0
	}

	tempfile=$(mktemp /tmp/$(basename $0).XXXXXX.sh)
	echo "Doing config discovery on $GCC_HOST ($GCC_HOSTNAME)"
	if do_on_remote "setup" 2>$tempfile.err >$tempfile
	then
		sed "s/^/remote: /" <$tempfile.err >&2
		rm $tempfile.err

		echo "A cat(1) of $tempfile:"
		sed 's/^/    /g' <$tempfile

		. $tempfile
		rm $tempfile
	else
		echo "Failed to do bootstrap on $GCC_HOST:"
		cat $tempfile.err
		exit 1
	fi

	if test -n "$REMOTE_REPO_PATH"
	then
		echo "Doing git push to $GCC_USER@$GCC_HOSTNAME:$REMOTE_REPO_PATH" >&2

		GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=accept-new"
		export GIT_SSH_COMMAND
		git push \
			${GIT_RECEIVE_PACK_PATH:+--receive-pack=$GIT_RECEIVE_PACK_PATH} \
			ssh://$GCC_USER@$GCC_HOSTNAME$REMOTE_REPO_PATH "$OPT_REVISION:refs/tags/ci-$OPT_REVISION"
	fi

	do_on_remote "make-test"
else
	# Config
	WHAT=
	DEBUG=
	PATH_PREFIX=
	NICE=19
	TESTS='t[0-9]*.sh'
	TAR=tar
	JOBS=1
	REMOTE_REPO_PATH=
	GIT_NOT_IN_PATH=
	PROVE_STATELESS=
	CMD_PRE_CHECKOUT=
	CMD_POST_CHECKOUT=
	CMD_BUILD=
	case $OPT_HOST in
		gcc10)
			JOBS=2
			;;
		gcc110)
			JOBS=2
			;;
		gcc110|gcc111|gcc112|gcc135)
			JOBS=4
			;;
		gcc119)
			JOBS=4
			;;
		gcc202)
			JOBS=4
			;;
	esac
	# Figure out if we'll need --receive-pack, "which git" on Solaris
	# also returns 0 if it can't find git...
	if ! git --version >/dev/null 2>&1
	then
		echo "No git in PATH here, will need a --receive-pack argument" >&2
		GIT_NOT_IN_PATH=t
	fi
	# Solaris has git in /opt/csw/bin/git and make in
	# /opt/csw/bin/gmake
	if test -d /opt/csw
	then
	   PATH=/opt/csw/gnu:$PATH:/opt/csw/bin
	   TAR=gtar
	fi
	if test -d /pro/bin
	then
		PATH=/pro/bin:$PATH
	fi

	# Options (some override config)
	## jobs
	if test -n "$OPT_WHAT"
	then
		WHAT=$OPT_WHAT
	fi
	if test -n "$OPT_DEBUG"
	then
		DEBUG=$OPT_DEBUG
	fi
	if test -n "$OPT_PATH_PREFIX"
	then
		PATH_PREFIX=$OPT_PATH_PREFIX
	fi
	if test -n "$OPT_JOBS"
	then
		JOBS=$OPT_JOBS
		MAKEFLAGS="${MAKEFLAGS:+$MAKEFLAGS }j$OPT_JOBS"
		export MAKEFLAGS
	fi
	if test -n "$OPT_TESTS"
	then
		if test "$TESTS" != "$OPT_TESTS"
		then
			PROVE_STATELESS=1
		fi
		TESTS=$OPT_TESTS
	fi
	if test -n "$OPT_PROVE_STATELESS"
	then
		PROVE_STATELESS=$OPT_PROVE_STATELESS
	fi
	if test -n "$OPT_REMOTE_REPO_PATH"
	then
		REMOTE_REPO_PATH=$OPT_REMOTE_REPO_PATH
	fi
	if test -n "$OPT_CMD_PRE_CHECKOUT"
	then
		CMD_PRE_CHECKOUT=$OPT_CMD_PRE_CHECKOUT
	fi
	if test -n "$OPT_CMD_POST_CHECKOUT"
	then
		CMD_POST_CHECKOUT=$OPT_CMD_POST_CHECKOUT
	fi
	if test -n "$OPT_CMD_BUILD"
	then
		CMD_BUILD=$OPT_CMD_BUILD
	fi

	## nice (and maybe ionice) level
	if test -n "$OPT_NICE"
	then
		NICE=$OPT_NICE
	fi
	set +e
	renice -n $NICE $$ >&2
	if test $NICE = 19
	then
		ionice -c 3 -p $$ >&2
	fi
	set -e

	case "$WHAT" in
		setup)
			NEW_PATH_PREFIX=
			case "$PATH_PREFIX" in
				auto)
					whoami=$(whoami)
					id_u=$(id -u)
					scratch_dir="/scratch/$whoami/"
					run_dir="/run/user/$id_u/"
					if test -n "$whoami" -a -d $scratch_dir
					then
						NEW_PATH_PREFIX=$scratch_dir
					elif test -n "$id_u" -a -d "$run_dir"
					then
						NEW_PATH_PREFIX=$run_dir
					else
						NEW_PATH_PREFIX="$(pwd)/"
					fi
					if test -n "$NEW_PATH_PREFIX"
					then
						echo "Discovered PATH_PREFIX = $NEW_PATH_PREFIX" >&2
					fi
					PATH_PREFIX=$NEW_PATH_PREFIX
					echo PATH_PREFIX=\"$PATH_PREFIX\"
					;;
				*)
					if test -n "$PATH_PREFIX" && ! test -d "$PATH_PREFIX"
					then
						mkdir -p "$PATH_PREFIX"
					fi
					;;
			esac

			if git_version=$(git --version)
			then
				echo REMOTE_GIT_VERSION=\"$git_version\"
				echo "Discovered REMOTE_GIT_VERSION = $git_version" >&2

				path_maybe_relative="${PATH_PREFIX}git.git"
				git init --bare "$path_maybe_relative" >&2
				REMOTE_REPO_PATH=$(cd "$path_maybe_relative" && pwd)
				echo REMOTE_REPO_PATH=\"$REMOTE_REPO_PATH\"
				echo "Discovered REMOTE_REPO_PATH = $REMOTE_REPO_PATH" >&2

				if test -n "$GIT_NOT_IN_PATH"
				then
					GIT_RECEIVE_PACK_PATH=$(which git-receive-pack)
					echo "Discovered GIT_RECEIVE_PACK_PATH = $GIT_RECEIVE_PACK_PATH" >&2
					echo GIT_RECEIVE_PACK_PATH=\"$GIT_RECEIVE_PACK_PATH\"
				fi
			else
				echo "PANIC: Can't find git, should always be here!" >&2
				exit 1
			fi

			exit
			;;
		make-test)
			;;
		*)
			echo "Need something to do" >&2
			exit 1
			;;
	esac

	# Setup worktree
	WORKTREE="${PATH_PREFIX}git"
	if git -C "$REMOTE_REPO_PATH" worktree list 2>/dev/null
	then
		# Main shared worktree, but leave the door open to other
		# worktrees...
		if ! test -d "$WORKTREE"
		then
			if ! git -C "$REMOTE_REPO_PATH" worktree add "$WORKTREE" "$OPT_REVISION"
			then
				# On git 1.9.4 on HP/UX not having the
				# HEAD is a bad repo, try again. Need
				# "master" because the HEAD symref is
				# set up for it.
				git -C "$REMOTE_REPO_PATH" update-ref refs/heads/master "$OPT_REVISION"
				git -C "$REMOTE_REPO_PATH" worktree add "$WORKTREE" "$OPT_REVISION"
			fi
		fi
	else
		echo "Do not have git-worktree, setting up a fake one in '$WORKTREE'" >&2
		if ! test -d "$WORKTREE"
		then
			git clone --shared "$REMOTE_REPO_PATH" "$WORKTREE"
		fi
	fi
	cd $WORKTREE

	if test -n "$CMD_PRE_CHECKOUT"
	then
		sh -c "$CMD_PRE_CHECKOUT"
	fi

	# Would do -C earlier here, but e.g. git 1.8.* on gcc110 doesn't
	# know about -C.
	git checkout "$OPT_REVISION"

	# Find a working 'make'
	MAKE=make
	if gmake --version >/dev/null 2>&1
	then
		MAKE=gmake
	fi

	if test -n "$CMD_POST_CHECKOUT"
	then
		sh -c "$CMD_POST_CHECKOUT"
	fi

	# Configure
	cat >config.mak.new <<-\EOF
	DEVELOPER = 1
	GIT_TEST_OPTS =
	GIT_TEST_OPTS += --no-chain-lint
	GIT_TEST_OPTS += --verbose-log
	GIT_TEST_OPTS += --immediate
	GIT_TEST_OPTS += -x

	DEFAULT_TEST_TARGET = prove
	GIT_PROVE_OPTS =
	EOF

	# Deal with various prove versions and semantics
	if prove --help 2>&1 | grep -q -- --jobs
	then
		cat >>config.mak.new <<-EOF
		GIT_PROVE_OPTS += --jobs $JOBS
		EOF
	fi
	if prove --help 2>&1 | grep -q -- --timer
	then
		cat >>config.mak.new <<-\EOF
		GIT_PROVE_OPTS += --timer
		EOF
	fi
	if test -z "$PROVE_STATELESS"
	then
		cat >>config.mak.new <<-\EOF
		GIT_PROVE_OPTS += --state=failed,slow,save
		EOF
	fi

	# Tests to skip
	GIT_SKIP_TESTS=t7900.35

	# Per-host configuration
	case $OPT_HOST in
		gcc12|gcc16|gcc20|gcc68|gcc75|gcc114|gcc116|gcc202):
			echo 'NO_CURL=UnfortunatelyNot' >>config.mak.new
			;;
		gcc10|gcc14)
			echo 'NO_UNCOMPRESS2=YesHaveOldZlib' >>config.mak.new
			;;
		gcc22)
			echo 'NO_UNCOMPRESS2 = NeededForReftable' >>config.mak.new
			echo 'DEVOPTS=no-pedantic' >>config.mak.new # gcc 4.6.3, has no -Wpedantic
			echo 'CFLAGS += -std=gnu99' >>config.mak.new
			;;
		gcc45)
			echo 'NO_UNCOMPRESS2 = NeededForReftable' >>config.mak.new
			echo 'CFLAGS += -std=gnu99' >>config.mak.new
			;;
		gcc135)
			echo 'NO_CURL=UnfortunatelyNot' >>config.mak.new
			echo 'CFLAGS += -std=gnu99' >>config.mak.new
			echo 'NO_UNCOMPRESS2 = NeededForReftable' >>config.mak.new
			;;
		gcc111|gcc119)
			echo 'DEVOPTS=no-pedantic' >>config.mak.new
			echo 'NO_UNCOMPRESS2 = NeededForReftable' >>config.mak.new
			echo 'SHELL_PATH=/bin/bash' >>config.mak.new
			echo 'CC=xlc' >>config.mak.new
			## TODO: Once DEVELOPER=1 works
			#test $OPT_HOST = 'gcc111' && echo 'CFLAGS=-g -O0 -qmaxmem=524288' >>config.mak.new
			# https://www.ibm.com/docs/cs/xl-c-and-cpp-aix/16.1?topic=descriptions-qhalt-werror
			#test $OPT_HOST = 'gcc119' && echo 'CFLAGS=-g -O0 -qmaxmem=524288 -qhalt=i' >>config.mak.new
			echo 'CFLAGS=-g -O0 -qmaxmem=524288' >>config.mak.new
			## END TODO
			echo 'NO_CURL=UnfortunatelyNot' >>config.mak.new
			test $OPT_HOST = 'gcc111' && echo 'NO_GETTEXT=HaveNoMsgfmt' >>config.mak.new
			;;
		gcc118)
			echo 'NO_OPENSSL=UnfortunatelyNot' >>config.mak.new
			echo 'NO_CURL=UnfortunatelyNot' >>config.mak.new
			;;
		gcc210|gcc211)
			echo 'SHELL_PATH=/bin/bash' >>config.mak.new

			test $OPT_HOST = 'gcc210' && echo 'NO_UNCOMPRESS2 = NeededForReftable' >>config.mak.new

			test $OPT_HOST = 'gcc210' && echo 'CC=/opt/developerstudio12.6/bin/suncc' >>config.mak.new
			test $OPT_HOST = 'gcc211' && echo 'CC=/opt/developerstudio12.6/bin/suncc' >>config.mak.new

			test $OPT_HOST = 'gcc210' && echo 'NO_OPENSSL=UnfortunatelyNot' >>config.mak.new
			test $OPT_HOST = 'gcc210' && echo 'NO_CURL=UnfortunatelyNot' >>config.mak.new

			echo 'NO_ICONV=HasLinkerErrors' >>config.mak.new
			echo 'NO_GETTEXT=HaveNoMsgfmt' >>config.mak.new
			;;
		gcc300)
			echo 'NO_ICONV=utf8.cErrorPassingArgument2OfIconvFromIncompatiblePointer' >>config.mak.new
			echo 'PERL_PATH=/usr/pkg/bin/perl' >>config.mak.new
			;;
		gcc301)
			echo 'NO_REGEX=NeedsStartEnd' >>config.mak.new
			echo 'NO_GETTEXT=Nope' >>config.mak.new
			echo 'NO_CURL=UnfortunatelyNot' >>config.mak.new
			;;
		gcc303)
			echo 'PYTHON_PATH=/usr/bin/python' >>config.mak.new
			echo 'PERL_PATH=/usr/local/bin/perl' >>config.mak.new
			;;
		gcc304)
			echo 'NO_GETTEXT=no-libintl.h' >>config.mak.new
			;;
		gcc400|gcc401)
			# dir.c:3085:13: error: 'memchr' specified
			# bound [9223372036854775808, 0] exceeds
			# maximum object size 9223372036854775807
			# [-Werror=stringop-overread]
			echo 'DEVOPTS=no-error' >>config.mak.new
			;;
		# avar@x2-ext.procura.nl
		x2-ext)
			#export LDOPTS="+s +DA64  -L/usr/local/lib/hpux64 -L/usr/local/lib -L/usr/lib/hpux64 -L/pro/local/lib -L/pro/lib -L/usr/lib"
			#echo "CFLAGS=-g -O0" >>config.mak.new
			#echo "NO_REGEX=MissingRegStartEnd" >>config.mak.new
			echo 'NO_CURL=UnfortunatelyNot' >>config.mak.new
			echo 'NO_GETTEXT=no-libintl.h' >>config.mak.new
			;;

	esac
	export GIT_SKIP_TESTS

	# Did the config.mak configuration change?
	if ! test -e config.mak
	then
		echo "Have new build directory, no old config.mak" >&2
		cp config.mak.new config.mak
	elif ! cmp config.mak config.mak.new >/dev/null 2>&1
	then
		echo "Have changes to config.mak, using new version" >&2
		diff -u config.mak config.mak.new || :
		mv config.mak.new config.mak
	fi

	# Build-only?
	if test -n "$CMD_BUILD"
	then
		echo "Building with custom build command '$CMD_BUILD'"
		sh -c "$CMD_BUILD"
	fi

	# Test environment
	TEST_NO_MALLOC_CHECK=1
	export TEST_NO_MALLOC_CHECK

	# Build + Test?
	if test "$OPT_TESTS" = "false"
	then
		echo "Skipping all tests with OPT_TESTS=$OPT_TESTS"
		$MAKE all
	elif test -n "$CMD_BUILD"
	then
		# Test-only due to CMD_BUILD
		echo "Testing '$OPT_REVISION' on '$OPT_HOST' with '$MAKEFLAGS' and '$OPT_TESTS'"
		$MAKE -C t T="$TESTS"
	else
		# Test
		echo "Testing '$OPT_REVISION' on '$OPT_HOST' with '$MAKEFLAGS' and '$OPT_TESTS'"
		if test -n "$OPT_TESTS"
		then
			$MAKE test T="$OPT_TESTS"
		else
			$MAKE test
		fi
	fi

	# Failed tests? TODO: Use upstream'd ci/print-test-failures.sh
	bad=
	for TEST_EXIT in $(egrep -l -x -v 0 test-results/*.exit)
	do
		bad=t
		TEST_OUT="${TEST_EXIT%exit}out"
		echo "------------------------------------------------------------------------"
		# TODO: cat, but not so firehose verbose, limit lines?
		echo "$TEST_OUT"
		echo "------------------------------------------------------------------------"
		../git version --build-options
	done
	if test -n "$bad"
	then
		exit 1
	fi
fi
